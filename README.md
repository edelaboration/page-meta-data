# User account

## Codeception tests

First install composer dependencies. Run `bin/composer install --prefer-dist`.
Codeception tests run via composer command `test`. See the scripts section of `composer.json` file.
Example:

```
$ bin/composer test
```