<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace unit\Services;

use RobotE13\PageMeta\Repositories\InMemoryPageRepository;
use RobotE13\PageMeta\Services\MetaTag\MetaTagDTO;
use RobotE13\PageMeta\Entities\MetaTag\MetaTag;
use RobotE13\PageMeta\Services\Page\{
    PageDTO,
    PageService
};

/**
 * Class PageServiceTest
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class PageServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        $this->service = new PageService(new InMemoryPageRepository());
        $this->pageDto = new PageDTO();
        $this->pageDto->route = 'test';
        $this->pageDto->routeParams = [];
        $this->pageDto->metaTags = [];
        $this->pageDto->blocks = [];
        $this->pageDto->canonical = '';
    }

    public function testCreatePage()
    {
        $pageDto = new PageDTO();
        $pageDto->route = 'testpage';
        $pageDto->routeParams = [];
        $pageDto->metaTags = [];
        $pageDto->canonical = '';
        $page = $this->service->create($pageDto);
        expect('Create Page', $page->getCurrentPage()->getRoute())->equals($pageDto->route);

        $pageDto2 = new PageDTO();
        $pageDto2->route = 'testpage2';
        $pageDto2->routeParams = [];
        $pageDto2->metaTags = [];
        $pageDto2->canonical = '';
        $page = $this->service->create($pageDto2);
        expect('Create Page 2', $page->getCurrentPage()->getRoute())->equals($pageDto2->route);

        $uid2 = $page->getCurrentPage()->getUid();
        $page = $this->service->setPageById($uid2);
        expect('Change current page on second by Id', $page->getCurrentPage()->getRoute())->equals($pageDto2->route);

        expect('Добавлено 2 страницы', count($this->service->getPages()->findAll()['items']))->equals(2);

        $this->service->remove();
        expect('Удалена 1 страница', count($this->service->getPages()->findAll()['items']))->equals(1);
    }

    /**
     * @todo Неверно, что в dto отдается готовая сущность. В dto должны попадать данные
     * (например в виде массива), а уже сервис должен создавать нужные сущности.
     */
    public function testCreatePageWithParams()
    {
        $pageDto = new PageDTO();
        $pageDto->route = 'testpage';
        $pageDto->routeParams = ['category' => 'test'];
        $pageDto->metaTags = [new MetaTag('keywords', 'test')];
        $pageDto->htmlBlocks = [new \RobotE13\PageMeta\Entities\HTMLBlock\HtmlBlock('block1', '<p>content</p>')];
        $pageDto->canonical = '<link rel="canonical" href="https://example.com/sample-page/" />';
        $page = $this->service->create($pageDto);
        expect('Create Page', $page->getCurrentPage()->getRoute())->equals($pageDto->route);
        expect('Create Page check params', $page->getCurrentPage()->getRouteParams())->equals($pageDto->routeParams);
        expect('Create Page check metaTags', array_values($page->getCurrentPage()->getMetaTags()->toArray()))
            ->equals($pageDto->metaTags);
        expect('Create Page check canonical', $page->getCurrentPage()->getCanonical())->equals($pageDto->canonical);
    }

    public function testMetaTags()
    {
        $this->service->create($this->pageDto);

        $MetaTagDto = new MetaTagDTO();
        $MetaTagDto->attributeClass = 'MetaTag';
        $MetaTagDto->name = 'keywords';
        $MetaTagDto->content = 'service test';
        $MetaTagDto->attributes = [];

        $metatags = $this
            ->service
            ->addMetaTag($MetaTagDto)
            ->getMetaTags()
            ->toArray();
        expect('Добавили MetaTag', $this->service->getMetaTags()->count())->equals(1);
        expect('Добавили MetaTag (name)', $metatags[array_key_first($metatags)]->getName())->same('keywords');
        expect('Добавили MetaTag (content)', $metatags[array_key_first($metatags)]->getContent())->same('service test');

        $this->service->removeMetaTag($MetaTagDto);
        expect('Удалили MetaTag (name)', $this->service->getMetaTags()->count())->equals(0);
    }

    public function testPropertyMetaTags()
    {
        $this->service->create($this->pageDto);

        $MetaTagDto = new MetaTagDTO();
        $MetaTagDto->attributeClass = 'PropertyMetaTag';
        $MetaTagDto->name = 'og:description';
        $MetaTagDto->content = 'Some description';
        $MetaTagDto->attributes = [];

        $metatags = $this
            ->service
            ->addMetaTag($MetaTagDto)
            ->getMetaTags()
            ->toArray();
        expect('Добавили PropertyMetaTag (property)', $metatags[array_key_first($metatags)]->getProperty())->same('og:description');
        expect('Добавили PropertyMetaTag (content)', $metatags[array_key_first($metatags)]->getContent())->same('Some description');

        $MetaTagDto->content = 'Changed description';
        $metatags = $this
            ->service
            ->updateMetaTag($MetaTagDto)
            ->getMetaTags()
            ->toArray();
        expect('Изменили PropertyMetaTag (content)', $metatags[array_key_first($metatags)]->getContent())->same('Changed description');
    }
}
