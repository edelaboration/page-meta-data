<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace tests\unit\Entities;

use RobotE13\DDD\Entities\Uuid\Id;
use RobotE13\PageMeta\Entities\Page;

/**
 * Class PageTest
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class PageTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {

    }

    protected function _after()
    {

    }

    // tests
    public function testSuccessfullCreate()
    {
        $uuid = Id::next();
        $page = $this->tester->getPageBuilder()->withUid($uuid)->create();

        expect('UUID созданной страницы совпадает с заданным UUID', $page->getUid()->isEqualTo($uuid))->true();
        expect('Immutable DateTime object', $page->getCreatedAt())->isInstanceOf(\DateTimeImmutable::class);
    }

    public function testCannotAddExisting(): void
    {
        //$this->expectException(\InvalidArgumentException::class);
        //$this->expectErrorMessage('MetaTag already exist.');

        $page1 = $this->tester->getPageBuilder()->create();
        $page2 = $this->tester->getPageBuilder()->create();

        //$page->addMetaTag($this->tester->getMetaTagBuilder()->create());
        //$page->addMetaTag($this->tester->getMetaTagBuilder()->create());
    }

    public function testFailCreate()
    {
        $uuid = Id::next();

        expect('Объект не будет создан если не заданы параметры.', fn() => new Page())
            ->throws(\ArgumentCountError::class);
    }

}
