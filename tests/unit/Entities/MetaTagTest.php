<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace tests\unit\Entities;

/**
 * Class MetaTagTest
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class MetaTagTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testAdd(): void
    {
        $page = $this->tester->getPageBuilder()->create();
        $page->addMetaTag($this->tester->getMetaTagBuilder()->create());

        expect('Добавлен 1 metatag', count($page->getMetaTags()))->equals(1);
    }

    public function testCannotAddExisting(): void
    {
        //$this->expectException(\InvalidArgumentException::class);
        $this->expectErrorMessage('MetaTag already exist.');

        $page = $this->tester->getPageBuilder()->create();

        $page->addMetaTag($this->tester->getMetaTagBuilder()->create());
        $page->addMetaTag($this->tester->getMetaTagBuilder()->create());
    }

    public function testRemoveByTag(): void
    {
        $page = $this->tester->getPageBuilder()->create();

        $page->addMetaTag($metatag1 = $this->tester->getMetaTagBuilder()->create());
        $page->addMetaTag($metatag2 = $this->tester->getMetaTagBuilder()->withName('author')->withContent('Sergey Bakhtin')->create());
        $page->removeMetaTag($metatag2);
        expect('После удаления осталcя 1 метатег', count($page->getMetaTags()))->equals(1);

        $lastMetatag = $page->getMetaTags()->toArray();
        expect('Оставшийся metatag', $lastMetatag[array_key_first($lastMetatag)])->same($metatag1);
    }

    public function testRemoveByIndex(): void
    {
        $page = $this->tester->getPageBuilder()->create();

        $page->addMetaTag($metatag = $this->tester->getMetaTagBuilder()->create());
        $page->addMetaTag($this->tester->getMetaTagBuilder()->withName('author')->withContent('Sergey Bakhtin')->create());
        expect('Добавлено 2 метатега', count($page->getMetaTags()))->equals(2);

        $page->removeMetaTagByIndex(array_key_last($page->getMetaTags()->toArray()));
        expect('После удаления осталcя 1 метатег', count($page->getMetaTags()))->equals(1);

        $lastMetatag = $page->getMetaTags()->toArray();
        expect('Оставшийся metatag', $lastMetatag[array_key_first($lastMetatag)])->same($metatag);
    }

    public function testRemoveNotExist(): void
    {
        $page = $this->tester->getPageBuilder()->create();

        //by name
        $metatag = $this->tester->getMetaTagBuilder()->create();
        expect('', fn() => $page->removeMetaTag($metatag))
            ->throws(\Webmozart\Assert\InvalidArgumentException::class,
                'MetaTag '.$metatag->getName().' not present in collection.');

        //by index
        expect('', fn() => $page->removeMetaTagByIndex(111))
            ->throws(\Webmozart\Assert\InvalidArgumentException::class,
                'MetaTag with key `111` not present in collection.');
    }

    public function testUpdate(): void
    {
        //equals
        $metatag1 = $this->tester->getMetaTagBuilder()->create();
        $metatag2 = $this->tester->getMetaTagBuilder()->create();
        expect($metatag1)->equals($metatag2);

        //not equals
        $metatag2->updateContent('new update');
        expect($metatag1)->notEquals($metatag2);

        $page = $this->tester->getPageBuilder()->create();

        $page->addMetaTag($this->tester->getMetaTagBuilder()->withName('author')->withContent('Evgenii Dudal')->create());
        $updatedMetatag = $this->tester->getMetaTagBuilder()->withName('author')->withContent('Sergey Bakhtin')->create();
        $page->updateMetaTag($updatedMetatag);
        $updatedMetatag = $page->getMetaTags()->toArray();
        expect('Обновление метатега в странице', $updatedMetatag[array_key_first($updatedMetatag)]->getContent())->equals('Sergey Bakhtin');
    }

    public function testMetaTagAttributes(): void
    {
        $metatag = $this->tester->getMetaTagBuilder()->withName('copyright')->withContent('JZ WEB')->create();
        $metatag->setAttribute('lang','ru');
        expect('To string with attributes', $metatag)->equals('<meta name="copyright" content="JZ WEB" lang="ru">');

        expect('Получение атрибутов', $metatag->getAttributes())->same(['lang'=>'ru']);

        $metatag->removeAttribute('lang');
        expect('To string with attributes after remove', $metatag)->equals('<meta name="copyright" content="JZ WEB">');
    }

    public function testToString(): void
    {
        $metatag = $this->tester->getMetaTagBuilder()->withName('keywords')->withContent('test')->create();

        expect('To string', $metatag)->equals('<meta name="keywords" content="test">');
    }
}
