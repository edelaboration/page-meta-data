<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace unit\Repositories;

use RobotE13\PageMeta\Tests\PageRepositoryAbstractTest;
use RobotE13\PageMeta\Repositories\InMemoryPageRepository;

/**
 * Class InMemoryPageRepositoryTest
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class InMemoryPageRepositoryTest extends PageRepositoryAbstractTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        $this->repository = new InMemoryPageRepository();
    }
}
