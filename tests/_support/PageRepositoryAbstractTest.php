<?php

/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Tests;

use RobotE13\DDD\Entities\Uuid\Id;
use RobotE13\PageMeta\Repositories\{
    PageRepository,
    NotFoundException
};
use RobotE13\PageMeta\Entities\Page;

/**
 * Class PageRepositoryAbstractTest
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
abstract class PageRepositoryAbstractTest extends \Codeception\Test\Unit
{

    /**
     *
     * @var PageRepository
     */
    protected $repository;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testAdd()
    {
        $id = Id::next();
        $page = $this->tester->getPageBuilder()
                ->withUid($id)
                ->withRoute('test')
                ->withRouteParams(['category' => 'sample-page'])
                ->withMetaTags([new \RobotE13\PageMeta\Entities\MetaTag\MetaTag('metatag1', 'content1')])
                ->withBlocks([new \RobotE13\PageMeta\Entities\HTMLBlock\HtmlBlock('block1', 'content1')])
                ->withCanonical('<link rel="canonical" href="https://example.com/sample-page/" />')
                ->create();

        $this->repository->add($page);
        $pageFound = $this->repository->findById($id);

        expect('Uid объекта полученного из репозитория совпадает с uid сохраненного объекта',
                        $page->getUid()->isEqualTo($pageFound->getUid()))
                ->true();
        expect('Route объекта полученного из репозитория совпадает с route сохраненного объекта',
                        $page->getRoute() === $pageFound->getRoute())
                ->true();
        expect('Страница содержит `block1` со значением `content1` ',
                        $pageFound->getHtmlBlocks()->get('block1')->getContent())
                ->equals('content1');
        expect('Страница содержит `metatag1` со значением `content1` ',
                        $pageFound->getMetaTags()->get(hash('crc32', 'metatag1'))->getContent())
                ->equals('content1');
        expect('Параметр заданный в исходном объекте имеется в полученном из репозитория',
                        $pageFound->getRouteParams())
                ->contains('sample-page');
        expect('Canonical link совпадает с canonical исходного объекта',
                        $pageFound->getCanonical())
                ->equals($page->getCanonical());
    }

    public function testRemove()
    {
        $id = Id::next();

        $page = $this->tester->getPageBuilder()->withUid($id)->create();
        $this->repository->add($page);

        expect('Страница найдена по ID и UID найденного совпадает с UID ранее добавленного',
                        $this->repository->findById($id)
                        ->getUid()
                        ->isEqualTo($id))
                ->true();

        $this->repository->remove($page->getUid());

        expect('Not found', fn() => $this->repository->findById($id))
                ->throws(NotFoundException::class);
    }

    public function testFind()
    {
        $id = Id::next();
        $route = 'test';

        $page = $this->tester->getPageBuilder()
                ->withUid($id)
                ->withRoute($route)
                ->create();
        $this->repository->add($page);

        expect('Страница найдена по ID и UID найденного совпадает с UID ранее добавленного',
                        $this->repository->findById($id)
                        ->getUid()
                        ->isEqualTo($id))
                ->true();

        expect('Страница найдена по route и UID найденного совпадает с UID ранее добавленного',
                        $this->repository->findByRoute($route)
                        ->getUid()
                        ->isEqualTo($id))
                ->true();

        $id = Id::next();
        $route = 'test2';
        $routeParams = ['category' => 'sample-page2'];

        $page2 = $this->tester->getPageBuilder()
                ->withUid($id)
                ->withRoute($route)
                ->withRouteParams($routeParams)
                ->create();
        $this->repository->add($page2);
        expect('Страница найдена по route c параметрами и UID найденного совпадает с UID ранее добавленного',
                        $this->repository->findByRouteWithParams($route, $routeParams)
                        ->getUid()
                        ->isEqualTo($id))
                ->true();

        expect('Получение списка записей страниц',
                        $this->repository->findAll()['items'])
                ->containsOnlyInstancesOf(Page::class);
    }

    public function testNotFound()
    {
        expect('Not found', fn() => $this->repository->findById(Id::next()))
                ->throws(NotFoundException::class);

        $id = Id::next();
        $route = 'test';

        $page = $this->tester->getPageBuilder()
                ->withUid($id)
                ->withRoute($route)
                ->create();
        $this->repository->add($page);

        expect('Not found', fn() => $this->repository->findByRoute('not_exist'))
                ->throws(NotFoundException::class);

        expect('Not found', fn() => $this->repository->findByRouteWithParams('test', ['category' => 'new']))
                ->throws(NotFoundException::class);
    }

}
