<?php

namespace RobotE13\PageMeta\Tests\Builders;

use RobotE13\PageMeta\Entities\MetaTag\PropertyMetaTag;

class PropertyMetaTagBuilder
{
    private $property;
    private $content;

    public function __construct()
    {
        $this->property = "og:title";
        $this->content = "Заголовок страницы";
    }

    /**
     * Задать тегу свойство
     * @param $property
     *
     * @return $this
     */
    public function withProperty($property):self
    {
        return $this->getClone('property', $property);
    }

    /**
     * Задать content
     * @param $content
     *
     * @return $this
     */
    public function withContent($content):self
    {
        return $this->getClone('content', $content);
    }

    /**
     * Create PropertyMetaTag
     * @return PropertyMetaTag
     */
    public function create(): PropertyMetaTag
    {
        return new PropertyMetaTag($this->property, $this->content);
    }

    /**
     * Clone object
     * @param string $attribute
     * @param mixed $value
     * @return \self
     */
    private function getClone($attribute, $value): self
    {
        $clone = clone $this;
        $clone->{$attribute} = $value;
        return $clone;
    }
}
