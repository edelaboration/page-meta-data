<?php

namespace RobotE13\PageMeta\Tests\Builders;

use RobotE13\PageMeta\Entities\MetaTag\MetaTag;

class MetaTagBuilder
{
    private $name;
    private $content;

    public function __construct()
    {
        $this->name = "keywords";
        $this->content = "википедия, энциклопедия";
    }

    /**
     * Задать тегу имя
     * @param $name
     *
     * @return $this
     */
    public function withName($name):self
    {
        return $this->getClone('name', $name);
    }

    /**
     * Задать content
     * @param $content
     *
     * @return $this
     */
    public function withContent($content):self
    {
        return $this->getClone('content', $content);
    }

    /**
     * Create MetaTag
     * @return MetaTag
     */
    public function create(): MetaTag
    {
        return new MetaTag($this->name, $this->content);
    }

    /**
     * Clone object
     * @param string $attribute
     * @param mixed $value
     * @return \self
     */
    private function getClone($attribute, $value): self
    {
        $clone = clone $this;
        $clone->{$attribute} = $value;
        return $clone;
    }
}
