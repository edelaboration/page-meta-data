<?php

namespace RobotE13\PageMeta\Tests\Builders;

use RobotE13\DDD\Entities\Uuid\Id;
use RobotE13\PageMeta\Entities\Page;
use RobotE13\PageMeta\Entities\HTMLBlock\{
    HtmlBlock,
    HtmlBlocksCollection
};
use RobotE13\PageMeta\Entities\MetaTag\{
    MetaTag,
    MetaTagsCollection
};

class PageBuilder
{

    private $uuid;
    private $route;
    private $routeParams;
    private $metaTags;
    private $blocks;
    private $canonical;

    public function __construct()
    {
        $this->uuid = Id::next();
        $this->route = 'test';
        $this->routeParams = [];
        $this->metaTags = [];
        $this->blocks = [];
        $this->canonical = '';
    }

    /**
     * Задать создаваемой странице определенный ID
     * @param Id $uid
     * @return $this
     */
    public function withUid(Id $uid): self
    {
        return $this->getClone('uuid', $uid);
    }

    /**
     * Задать создаваемой странице route
     * @param string $route
     * @return $this
     */
    public function withRoute(string $route): self
    {
        return $this->getClone('route', $route);
    }

    /**
     * Задать создаваемой странице $_GET параметры route
     * @param array $routeParams
     * @return $this
     */
    public function withRouteParams(array $routeParams): self
    {
        return $this->getClone('routeParams', $routeParams);
    }

    /**
     * Задать создаваемой странице canonical link
     * @param string $canonical
     * @return $this
     */
    public function withCanonical(string $canonical): self
    {
        return $this->getClone('canonical', $canonical);
    }

    /**
     * Задать создаваемой странице meta tags
     * @param array $metaTags
     * @return $this
     */
    public function withMetaTags(array $metaTags): self
    {
        return $this->getClone('metaTags', $metaTags);
    }

    /**
     * Задать создаваемой странице html блоки.
     * @param array $blocks
     * @return $this
     */
    public function withBlocks(array $blocks): self
    {
        return $this->getClone('blocks', $blocks);
    }

    /**
     * Clone object
     * @param string $attribute
     * @param mixed $value
     * @return \self
     */
    private function getClone($attribute, $value): self
    {
        $clone = clone $this;
        $clone->{$attribute} = $value;
        return $clone;
    }

    public function create(): Page
    {
        return new Page(
                $this->uuid,
                $this->route,
                $this->routeParams,
                $this->metaTags,
                $this->blocks,
                $this->canonical
        );
    }

}
