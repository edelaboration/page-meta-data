<?php

namespace RobotE13\PageMeta\Tests\Module;

use RobotE13\PageMeta\Tests\Builders\{
    PageBuilder,
    MetaTagBuilder,
    PropertyMetaTagBuilder
};

/**
 * Builder
 */
class Builder extends \Codeception\Module
{

    /**
     * @var RobotE13\PageMeta\Tests\Builders\PageBuilder
     */
    private $pageBuilder;

    /**
     * @var RobotE13\PageMeta\Tests\Builders\MetaTagBuilder
     */
    private $metaTagBuilder;

    public function _beforeSuite($settings = array())
    {
        $this->pageBuilder = new PageBuilder();
        $this->metaTagBuilder = new MetaTagBuilder();
        $this->propertyMetaTagBuilder = new PropertyMetaTagBuilder();
    }

    /**
     *
     * @return RobotE13\PageMeta\Tests\Builders\PageBuilder
     */
    public function getPageBuilder(): PageBuilder
    {
        return $this->pageBuilder;
    }

    /**
     *
     * @return RobotE13\PageMeta\Tests\Builders\MetaBuilder
     */
    public function getMetaTagBuilder(): MetaTagBuilder
    {
        return $this->metaTagBuilder;
    }

    /**
     *
     * @return RobotE13\PageMeta\Tests\Builders\PropertyMetaBuilder
     */
    public function getPropertyMetaTagBuilder(): PropertyMetaTagBuilder
    {
        return $this->propertyMetaTagBuilder;
    }
}
