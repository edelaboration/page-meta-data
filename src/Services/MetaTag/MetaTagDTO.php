<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Services\MetaTag;

/**
 * Class MetaTagDTO
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class MetaTagDTO
{
    /**
     * @var string class name of metatag
     */
    public string $attributeClass;

    /**
     * @var string name of metatag
     */
    public string $name;

    /**
     * @var string content of metatag
     */
    public string $content;

    /**
     * @var array additional attributes of metatag
     */
    public array $attributes;

}
