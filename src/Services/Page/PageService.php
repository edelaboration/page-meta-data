<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Services\Page;

use Exception;
use RobotE13\DDD\Entities\Uuid\Id;
use RobotE13\PageMeta\Entities\Page;
use RobotE13\PageMeta\Entities\MetaTag\{
    MetaTag,
    PropertyMetaTag,
    MetaTagsCollection
};
use RobotE13\PageMeta\Services\MetaTag\MetaTagDTO;
use RobotE13\PageMeta\Repositories\{
    NotFoundException,
    PageRepository
};

/**
 * Class PageService
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class PageService
{
    /**
     * @var PageRepository
     */
    private PageRepository $pages;

    /**
     * @var Page
     */
    private Page $currentPage;

    /**
     * PageService constructor.
     *
     * @param  PageRepository  $pages
     */
    public function __construct(PageRepository $pages)
    {
        $this->pages = $pages;
    }

    /**
     * @param  PageDTO  $dto
     *
     * @return $this
     * @throws \Exception
     */
    public function create(PageDTO $dto)
    {
        $page = new Page(
            Id::next(),
            $dto->route,
            $dto->routeParams,
            $dto->metaTags,
            $dto->htmlBlocks,
            $dto->canonical
        );
        $this->pages->add($page);

        $this->currentPage = $page;

        return $this;
    }

    /**
     * Set current page by uid
     * @param  Id  $uid
     *
     * @return $this
     */
    public function setPageById(Id $uid): self
    {
        $this->currentPage = $this->pages->findById($uid);

        return $this;
    }

    /**
     * Get current page
     * @return Page
     */
    public function getCurrentPage(): Page
    {
        return $this->currentPage;
    }

    /**
     * @return PageRepository
     */
    public function getPages(): PageRepository
    {
        return $this->pages;
    }

    /**
     *  Add metatag
     * @param  MetaTagDTO  $metaTagDto
     *
     * @return $this
     */
    public function addMetaTag(MetaTagDTO $metaTagDto): self
    {
        if (isset($this->currentPage))
        {
            $this->currentPage->addMetaTag($this->resolveMetaTag($metaTagDto));
            $this->pages->update($this->currentPage);

            return $this;
        }
        throw new NotFoundException('Page not found.');
    }

    /**
     *  Get metatags collection of current page
     *
     * @return $this
     */
    public function getMetaTags(): MetaTagsCollection
    {
        if (isset($this->currentPage))
        {
            return $this->currentPage->getMetaTags();
        }
        throw new NotFoundException('Page not found.');
    }

    /**
     * Update metatag
     * @param  MetaTagDTO  $metaTagDto
     *
     * @return $this
     */
    public function updateMetaTag(MetaTagDTO $metaTagDto): self
    {
        if (isset($this->currentPage))
        {
            $this->currentPage->updateMetaTag($this->resolveMetaTag($metaTagDto));
            $this->pages->update($this->currentPage);

            return $this;
        }
        throw new NotFoundException('Page not found.');
    }

    /**
     * Remove metatag
     * @param  MetaTagDTO  $metaTagDto
     *
     * @return $this
     */
    public function removeMetaTag(MetaTagDTO $metaTagDto): self
    {
        if (isset($this->currentPage))
        {
            $this->currentPage->removeMetaTag($this->resolveMetaTag($metaTagDto));
            $this->pages->update($this->currentPage);

            return $this;
        }
        throw new NotFoundException('Page not found.');
    }

    /**
     * Remove current page
     * @return $this
     */
    public function remove(): self
    {
        if (isset($this->currentPage))
        {
            $this->pages->remove($this->currentPage->getUid());

            return $this;
        }
        throw new NotFoundException('Page not found.');
    }

    /**
     * resolve metaTag by DTO
     * @param  MetaTagDTO  $metaTagDto
     *
     * @return mixed
     */
    private function resolveMetaTag(MetaTagDto $metaTagDto)
    {
        $metaTagClass = "RobotE13\PageMeta\Entities\MetaTag\\$metaTagDto->attributeClass";
        if (class_exists($metaTagClass))
        {
            $metaTag = new $metaTagClass(
                $metaTagDto->name,
                $metaTagDto->content,
                $metaTagDto->attributes
            );

            return $metaTag;
        }

        throw new NotFoundException("Class {$metaTagClass} not found.");
    }
}
