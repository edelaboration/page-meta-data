<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta;

/**
 * Class ServiceLocator
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class ServiceLocator
{
    /**
     * @var self
     */
    private static $instance;

    /**
     * @return \self
     */
    public static function getInstance(): self
    {
        if(self::$instance === null)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
