<?php

/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Entities;

use RobotE13\DDD\Entities\Uuid\Id;
use RobotE13\PageMeta\Entities\HTMLBlock\{
    HtmlBlock,
    HtmlBlocksCollection
};
use RobotE13\PageMeta\Entities\MetaTag\{
    MetaTagsCollection,
    MetaTagInterface
};
use JsonSerializable;

/**
 * Description of Page
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class Page implements JsonSerializable
{

    /**
     * @var Id
     */
    private Id $uid;

    /**
     * @var \DateTimeImmutable createdAt
     */
    private \DateTimeImmutable $createdAt;

    /**
     * route страницы
     * @var $route
     */
    private string $route;

    /**
     * $_GET параметры страницы
     * @var $routeParams
     */
    private array $routeParams;
    private MetaTagsCollection $metaTags;
    private HtmlBlocksCollection $blocks;

    /**
     * Canonical link
     * @var $canonical
     */
    private string $canonical;

    /**
     * Page constructor.
     *
     * @param  Id  $uid uid страницы
     * @param  string  $route страницы
     * @param  array  $routeParams $_GET параметры страницы
     * @param  array  $metaTags
     * @param  HtmlBlock[]  $blocks
     * @param  string  $canonical canonical link
     *
     * @throws \Exception
     */
    public function __construct(
            Id $uid,
            string $route,
            array $routeParams = [],
            array $metaTags = [],
            array $blocks = [],
            string $canonical = ''
    )
    {
        $this->uid = $uid;
        $this->route = $route;
        $this->routeParams = $routeParams;
        $this->metaTags = new MetaTagsCollection($metaTags);
        $this->blocks = new HtmlBlocksCollection($blocks);
        $this->createdAt = new \DateTimeImmutable();
        if (!empty($canonical))
        {
            if (stristr($canonical, 'canonical') !== FALSE)
            {
                $this->canonical = $canonical;
            } else
            {
                throw new \Webmozart\Assert\InvalidArgumentException('Invalid Argument. Canonical not contains "canonical" string');
            }
        } else
        {
            $this->canonical = '';
        }
    }

    /**
     * @param $metatag
     *
     * @return $this
     */
    public function addMetaTag(MetaTagInterface $metatag)
    {
        $this->metaTags->add($metatag);

        return $this;
    }

    /**
     * Remove metatag
     * @param $metatag
     */
    public function removeMetaTag(MetaTagInterface $metatag)
    {
        $key = $this->metaTags->resolveIndexOf($metatag);
        try {
            $this->metaTags->remove($key);
        } catch (\InvalidArgumentException $e) {
            throw new \Webmozart\Assert\InvalidArgumentException('MetaTag ' . $metatag->getAttributeName() . ' not present in collection.');
        }
    }

    /**
     * Update metatag
     * @param $metatag
     * @return $this
     */
    public function updateMetaTag(MetaTagInterface $metatag): self
    {
        $key = $this->metaTags->resolveIndexOf($metatag);
        try {
            $this->metaTags->remove($key);
            $this->metaTags->add($metatag);
        } catch (\InvalidArgumentException $e) {
            throw new \Webmozart\Assert\InvalidArgumentException('MetaTag ' . $metatag->getAttributeName() . ' not present in collection.');
        }

        return $this;
    }

    /**
     * Remove MetaTag by index
     * @param $metatag
     */
    public function removeMetaTagByIndex(string $index)
    {
        $this->metaTags->remove($index);
    }

    //Getters

    /**
     * @return Id
     */
    public function getUid(): Id
    {
        return $this->uid;
    }

    /**
     * Дата создания
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Возращает route страницы
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @return array
     */
    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    /**
     * Возвращает canonical link
     * @return string
     */
    public function getCanonical(): string
    {
        return $this->canonical;
    }

    /**
     * Возвращает типизированную коллекцию тегов.
     * @return MetaTagsCollection
     */
    public function getMetaTags(): MetaTagsCollection
    {
        return $this->metaTags;
    }

    public function getHtmlBlocks(): HtmlBlocksCollection
    {
        return $this->blocks;
    }

    public function jsonSerialize()
    {
        return [
            'uid' => $this->uid->getString(),
            'route' => $this->route,
            'routeParams' => $this->routeParams,
            'metaTags' => $this->metaTags,
            'blocks' => $this->blocks,
            'canonical' => $this->canonical,
            'createdAt' => $this->createdAt,
        ];
    }
}
