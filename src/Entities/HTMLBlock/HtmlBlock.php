<?php

/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Entities\HTMLBlock;

use JsonSerializable;

/**
 * Description of HTMLBlock
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class HtmlBlock implements JsonSerializable
{

    /**
     * @var string
     */
    private $name;

    /**
     *
     * @var string
     */
    private $content;

    public function __construct(string $name, string $content)
    {
        $this->name = $name;
        $this->content = $content;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'content' => $this->content,
        ];
    }
}
