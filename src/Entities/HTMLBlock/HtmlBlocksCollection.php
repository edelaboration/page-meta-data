<?php

/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Entities\HTMLBlock;

use RobotE13\DDD\Entities\Collection\AbstractHashableCollection;

/**
 * Description of HtmlBlocksCollection
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class HtmlBlocksCollection extends AbstractHashableCollection
{
    public function getItemClass(): string
    {
        return HtmlBlock::class;
    }

    /**
     *
     * @param HtmlBlock $item
     * @return string
     */
    public function resolveIndexOf($item): string
    {
        return $item->getName();
    }

}
