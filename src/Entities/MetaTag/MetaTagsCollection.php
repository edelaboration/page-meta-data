<?php

/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Entities\MetaTag;

use RobotE13\DDD\Entities\Collection\AbstractHashableCollection;

/**
 * Class MetaTagsCollection
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class MetaTagsCollection extends AbstractHashableCollection
{

    const COLLECTION_ITEM_NAME = 'MetaTag';

    /**
     * @return string
     */
    public function getItemClass()
    {
        return MetaTag::class;
    }

    /**
     * Вычисляет ключ для переданного объекта - элемента коллекции
     * @param MetaTag $item
     *
     * @return string
     */
    public function resolveIndexOf($item): string
    {
        return hash('crc32', $item->getAttributeName());
    }

}
