<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Entities\MetaTag;

use JsonSerializable;

/**
 * Class MetaTag
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class MetaTag implements MetaTagInterface, JsonSerializable
{
    /**
     * @var string name of metatag
     */
    private string $name;

    /**
     * @var string content of metatag
     */
    private string $content;

    /**
     * @var array additional attributes of metatag
     */
    private array $attributes;

    /**
     * MetaTag constructor.
     *
     * @param string $name
     * @param string $content
     * @param array $attributes
     */
    public function __construct(string $name, string $content = "", array $attributes = [])
    {
        $this->name = $name;
        $this->content = $content;
        $this->attributes = $attributes;
    }

    /**
     * Get attribute name of property Metatag
     * @return string
     */
    public function getAttributeName(): string
    {
        return $this->getName();
    }

    /**
     * @param  MetaTag  $metaTag
     *
     * @return bool
     */
    public function isEqualTo(self $metaTag)
    {
        return $this->name === $metaTag->getName();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param $content
     *
     * @return $this
     */
    public function updateContent($content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get attributes
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * Set additional attribute
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function setAttribute(string $key, string $value): self
    {
        if (!isset($this->attributes))
        {
            $this->attributes = [];
        }

        $this->attributes[$key] = $value;

        return $this;
    }

    /**
     * Remove additional attribute
     * @param string $key
     *
     * @return $this
     */
    public function removeAttribute(string $key): self
    {
        if (isset($this->attributes[$key]))
        {
            unset($this->attributes[$key]);
        }

        return $this;
    }

    /**
     * Return metatag to html
     * @return string
     */
    public function __toString()
    {
        $attributes = '';
        if (isset($this->attributes))
        {
            foreach ($this->attributes as $attr => $value)
            {
                $attributes .= ' '.$attr.'="'.$value.'"';
            }
        }

        return '<meta name="'.$this->name.'" content="'.$this->content.'"'.$attributes.'>';
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'content' => $this->content,
            'attributes' => $this->attributes,
        ];
    }
}
