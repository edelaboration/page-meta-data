<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Entities\MetaTag;

/**
 * Interface MetaTagInterface
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
interface MetaTagInterface
{
    /**
     * Get the name of main attribute, saved in 'name', 'property', etc
     * @return string
     */
    public function getAttributeName(): string;
}
