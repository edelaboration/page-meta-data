<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Repositories;

use RobotE13\DDD\Entities\Uuid\Id;
use RobotE13\PageMeta\Entities\Page;

/**
 * Interface PageRepository
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
interface PageRepository
{
    /**
     * @param Id $uid
     * @return Page
     * @throws NotFoundException
     */
    public function findById(Id $uid): Page;

    /**
     * @param string $route
     * @return Page
     * @throws NotFoundException
     */
    public function findByRoute(string $route): Page;

    /**
     * @param string $route
     * @param array $routeParams
     * @return Page
     * @throws NotFoundException
     */
    public function findByRouteWithParams(string $route, array $routeParams): Page;

    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @param Page $page
     * @return void
     */
    public function add(Page $page): void;

    /**
     * @param Page $page
     * @return void
     */
    public function update(Page $page): void;

    /**
     * @param Id $uid UUID of the page
     * @return void
     * @throws NotFoundException
     */
    public function remove(Id $uid): void;
}
