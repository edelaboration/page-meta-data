<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Repositories;

/**
 * Class NotFoundException
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class NotFoundException extends \DomainException
{

}
