<?php
/**
 * This file is part of the page-meta-data.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package page-meta-data
 */

namespace RobotE13\PageMeta\Repositories;

use RobotE13\DDD\Entities\Uuid\Id;
use RobotE13\PageMeta\Entities\Page;

/**
 * Class InMemoryPageRepository
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class InMemoryPageRepository implements PageRepository
{

    /**
     * @var Page[]
     */
    private $items = [];

    /**
     * @inheritDoc
     */
    public function findById(Id $uid): Page
    {
        if(!isset($this->items[$uid->getString()]))
        {
            throw new NotFoundException('Page not found.');
        }
        return clone $this->items[$uid->getString()];
    }

    /**
     * @inheritDoc
     */
    public function findByRoute(string $route): Page
    {
        foreach ($this->items as $page)
        {
            if($page->getRoute() === $route && empty($page->getRouteParams()))
            {
                return $page;
            }
        }
        throw new NotFoundException('Page not found.');
    }

    /**
     * @inheritDoc
     */
    public function findByRouteWithParams(string $route, array $routeParams): Page
    {
        foreach ($this->items as $page)
        {
            if($page->getRoute() === $route && $page->getRouteParams() == $routeParams)
            {
                return $page;
            }
        }
        throw new NotFoundException('Page with params not found.');
    }

    /**
     * @inheritDoc
     */
    public function findAll(): array
    {
        return ['items' => $this->items];
    }

    /**
     * @inheritDoc
     */
    public function add(Page $page): void
    {
        $this->items[$page->getUid()->getString()] = $page;
    }

    /**
     * @inheritDoc
     */
    public function update(Page $page): void
    {
        $this->add($page);
    }

    /**
     * @inheritDoc
     */
    public function remove(Id $uid): void
    {
        $this->findById($uid);
        unset($this->items[$uid->getString()]);
    }
}
